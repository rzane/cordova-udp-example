# Cordova UDP Example

The interesting files are in `www/js/src/`. This project utilizes https://github.com/MobileChromeApps/cordova-plugin-chrome-apps-sockets-udp.

### Running

To run this project, run these commands:

+ `npm install -g cordova`
+ `npm install -g ios-sim`
+ `cordova emulate ios`