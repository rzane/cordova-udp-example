var application = {
  initialize: function() {
    // Grab some elements for later
    this.messages   = $('#messages');
    this.newMessage = $('#new-message');
    this.messageText = this.newMessage.find('input[type=text]');

    // Create a new server
    this.server = new UDP.Server('0.0.0.0');

    // Setup a callback for when a message is received.
    this.server.onReceive(this.messageReceived.bind(this));

    // Start the server. When it's done, call the callback.
    this.server.listen(this.serverStarted.bind(this));
  },

  // Callback for when a message is received.
  messageReceived: function(message, info) {
    this.append('Received', message);
  },

  // Callback for when the server has been successfully started.
  serverStarted: function(address, port) {
    // Now we're listening...
    this.append('Listening', address + ':' + port);

    // Initialize a client now that we know the UDP server's
    // address and port
    this.client = new UDP.Client(address, port);

    // Bind a submit event w/ callback on the form
    this.newMessage.on('submit', this.sendMessage.bind(this));
  },

  // Called when the form is submitted.
  sendMessage: function(event) {
    // Prevent the form from actually submitting
    event.preventDefault();

    // Get the value currently entered in the text box
    var message = this.messageText.val();

    // Send the message to the server and bind the callback
    this.client.send(message, this.messageSent.bind(this));
  },

  // Called when a message is sent. The info should tell you
  // if sending was successful.
  messageSent: function(info) {
    this.append('Sent', 'Result code was ' + info.resultCode);
  },

  // Add an item to the list on the page.
  append: function(name, info) {
    var item = $('<li>');
    item.append($('<strong>').html(name));
    item.append($('<span>').html(info));
    this.messages.append(item);
  }
};

$(document).on('deviceready', function() {
  application.initialize();
});
